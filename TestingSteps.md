RoundedImageView组件测试步骤

步骤一：安装应用到RK3568单板(OpenHarmony 3.2 Release版本)；

步骤二：打开应用,进入到首页，切换类型即可查看对应的显示效果；


RoundedImageView component test steps

Step 1: Install the application on the RK3568 board (OpenHarmony 3.2 Release version);

Step 2: Open the application, enter the homepage, switch the type to view the corresponding display effect;


